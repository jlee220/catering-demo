from django.urls import path

from packages.views import (
    PackageListView,
    PackageDetailView,
    PackageCreateView,
)

urlpatterns = [
    path("", PackageListView.as_view(), name="list_packages"),
    path("<int:pk>/", PackageDetailView.as_view(), name="show_package"),
    path("create/", PackageCreateView.as_view(), name="create_package"),
]
