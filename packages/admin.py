from django.contrib import admin
from packages.models import Package


class PackageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Package, PackageAdmin)
