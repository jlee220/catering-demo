# from django.urls import reverse_lazy
from django.shortcuts import redirect
from packages.models import Package
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin


class PackageListView(LoginRequiredMixin, ListView):
    model = Package
    template_name = "packages/list.html"

    def get_queryset(self):
        return Package.objects.filter(members=self.request.user)


class PackageDetailView(LoginRequiredMixin, DetailView):
    model = Package
    template_name = "packages/detail.html"


class PackageCreateView(LoginRequiredMixin, CreateView):
    model = Package
    template_name = "packages/create.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        package = form.save(commit=False)
        package.member_set = self.request.user
        package.save()
        form.save_m2m()
        return redirect("show_package", pk=package.id)
